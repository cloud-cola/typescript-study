# 在node中搭建TS开发环境

需要全局安装 ts-node  typescript  nodemon 
npm i -g ***   //（***为相关包的名称）

## 安装Typescript

默认情况下，TS会做一下几种假设：
1.假设当前的执行的环境是DOM（浏览器）环境
2.如果代码中没有使用模块化语句，比如import、export,便认为该代码是全局执行
3.编译的目标代码是es3

有两种方式更改以上假设
1.使用tsc命令行的时候，加上选项参数；
2.使用配置文件更改编译选项

## TS的配置文件
1.系统设置里添加（不推荐）
2.命令行   tsc --init
注意：添加了配置文件后，使用tsc命令时不能再带上文件名，否则会忽略配置文件

tsconfig.json：
{
  "compilerOptions": { //编译选项
    "target": "es2016", //es7 
    "module": "commonjs",
    "lib": [
      "es2016" //去掉浏览器环境，配置执行环境为node，但是需要安装第三方依赖文件@types/node
    ],
    "outDir": "./dist" //编译文件输出到哪个目录
  },
  "include": [
    "./src" //编译哪个目录下的TS文件
  ]
  //若是编译某个文件夹下的某个文件
  //"files":["./src/index.ts"]
}

package.json：
{
  "scripts": {
    "dev": "nodemon --watch src -e ts --exec ts-node src/index.ts"
  },
  "devDependencies": {
    "@types/node": "^14.14.25"
  }
}
.gitignore：
node_modules/
